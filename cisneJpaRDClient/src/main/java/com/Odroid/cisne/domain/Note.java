package com.Odroid.cisne.domain;

public class Note extends NoteMeta {
	private String content = "c";
	private Long version = 0L;

	public Note() {
	}

	public Note(Long id, String title, String content, long version) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.setVersion(version);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Note [content=" + content + ", version=" + version + ", id="
				+ id + ", title=" + title + "]";
	}

}
