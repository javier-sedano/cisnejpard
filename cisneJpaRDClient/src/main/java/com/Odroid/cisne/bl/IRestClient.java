package com.Odroid.cisne.bl;

import org.springframework.web.client.RestTemplate;

public interface IRestClient {
	public boolean setCredentials(String username, String password);

	public RestTemplate getRestTemplate();

}
