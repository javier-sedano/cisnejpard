package com.Odroid.cisne.bl;

import com.Odroid.cisne.domain.Note;
import com.Odroid.cisne.domain.NoteMeta;

public interface INotesService {

	public Long searchCount(String text);

	public NoteMeta searchGet(String text, Long index);

	public Long addEmptyNote();

	public Note get(Long id);

	public void save(Note note);

	public void delete(Long id, Long version);
}
