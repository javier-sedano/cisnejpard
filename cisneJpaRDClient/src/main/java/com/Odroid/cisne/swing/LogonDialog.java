package com.Odroid.cisne.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.Odroid.cisne.bl.IRestClient;
import com.Odroid.cisne.fw.swing.Dialog;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Toolkit;

@Dialog("logonDialog")
public class LogonDialog extends JDialog implements ILogon {
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private boolean logged = false;

	@Autowired
	@Qualifier("restClient")
	private IRestClient restClient;
	private JTextField textFieldUsername;
	private JTextField textFieldPassword;

	/**
	 * Create the dialog.
	 */
	@Autowired
	public LogonDialog(@Value("${server}") String server) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				LogonDialog.class.getResource("/res/cisne.png")));
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 413, 150);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWeights = new double[] { 0.0, 1.0 };
		gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0 };
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblUsername = new JLabel("Username:");
			lblUsername.setToolTipText("");
			GridBagConstraints gbc_lblUsername = new GridBagConstraints();
			gbc_lblUsername.anchor = GridBagConstraints.EAST;
			gbc_lblUsername.insets = new Insets(0, 0, 5, 5);
			gbc_lblUsername.gridx = 0;
			gbc_lblUsername.gridy = 0;
			contentPanel.add(lblUsername, gbc_lblUsername);
		}
		{
			textFieldUsername = new JTextField();
			textFieldUsername.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						tryLogin();
					} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						cancel();
					}
				}
			});
			GridBagConstraints gbc_textFieldUsername = new GridBagConstraints();
			gbc_textFieldUsername.insets = new Insets(0, 0, 5, 0);
			gbc_textFieldUsername.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldUsername.gridx = 1;
			gbc_textFieldUsername.gridy = 0;
			contentPanel.add(textFieldUsername, gbc_textFieldUsername);
			textFieldUsername.setColumns(10);
		}
		{
			JLabel lblPassword = new JLabel("Password:");
			GridBagConstraints gbc_lblPassword = new GridBagConstraints();
			gbc_lblPassword.anchor = GridBagConstraints.EAST;
			gbc_lblPassword.insets = new Insets(0, 0, 0, 5);
			gbc_lblPassword.gridx = 0;
			gbc_lblPassword.gridy = 1;
			contentPanel.add(lblPassword, gbc_lblPassword);
		}
		{
			textFieldPassword = new JTextField();
			textFieldPassword.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						tryLogin();
					} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						cancel();
					}
				}
			});
			GridBagConstraints gbc_textFieldPassword = new GridBagConstraints();
			gbc_textFieldPassword.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldPassword.gridx = 1;
			gbc_textFieldPassword.gridy = 1;
			contentPanel.add(textFieldPassword, gbc_textFieldPassword);
			textFieldPassword.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						tryLogin();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancel();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			JPanel headerPanel = new JPanel();
			getContentPane().add(headerPanel, BorderLayout.NORTH);
			{
				JLabel headerLabel = new JLabel("Connecting to " + server);
				headerPanel.add(headerLabel);
			}
		}
	}

	private void tryLogin() {
		String username = textFieldUsername.getText();
		String password = textFieldPassword.getText();
		logged = restClient.setCredentials(username, password);
		if (logged) {
			setVisible(false);
		} else {
			JOptionPane.showMessageDialog(this, "Wrong logon", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void cancel() {
		logged = false;
		this.setVisible(false);
	}

	@Override
	public boolean logon() {
		setVisible(true);
		return logged;
	}

}
