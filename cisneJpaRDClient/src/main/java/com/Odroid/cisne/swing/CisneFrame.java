package com.Odroid.cisne.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.domain.Note;
import com.Odroid.cisne.fw.swing.Frame;
import com.Odroid.cisne.swing.NotesListModel.NoteModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

@Frame("cisneFrame")
public class CisneFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("logonDialog")
	private ILogon logonDialog;

	@Autowired
	@Qualifier("notesService")
	private INotesService notesService;

	private NotesListModel notesListModel;

	private JTextField textFilter;
	private JTextField textTitle;
	private JTextArea textContent;
	private JList<NoteModel> listNotes;
	private JPanel panelMain;

	private int mainWidth = 300;
	private Long showingId = -1L;
	private Long showingVersion = -1L;

	@Autowired
	public CisneFrame(
			@Qualifier("notesListModel") NotesListModel notesListModel,
			@Qualifier("notesCellRenderer") NotesCellRenderer notesCellRenderer) {
		initGUI();
		this.notesListModel = notesListModel;
		listNotes.setPrototypeCellValue(new NoteModel(0L,
				"123456789a123456789b"));
		listNotes.setModel(notesListModel);
		listNotes.setCellRenderer(notesCellRenderer);
		textFilter.getDocument().addDocumentListener(filterChangeListener);
	}

	private void initGUI() {
		setTitle("swCisne");
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				LogonDialog.class.getResource("/res/cisne.png")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 424, 471);

		JSplitPane splitPane = new JSplitPane();
		getContentPane().add(splitPane, BorderLayout.CENTER);

		panelMain = new JPanel();
		panelMain.setVisible(false);
		splitPane.setRightComponent(panelMain);
		panelMain.setLayout(new BorderLayout(0, 0));

		JPanel panelDetailsButtons = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelDetailsButtons.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panelMain.add(panelDetailsButtons, BorderLayout.NORTH);

		JButton btnSave = new JButton("");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveNote();
			}
		});
		btnSave.setIcon(new ImageIcon(CisneFrame.class
				.getResource("/com/Odroid/cisne/swing/img/accept.png")));
		panelDetailsButtons.add(btnSave);

		JButton btnDelete = new JButton("");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteNote();
			}
		});
		btnDelete.setIcon(new ImageIcon(CisneFrame.class
				.getResource("/com/Odroid/cisne/swing/img/delete.png")));
		panelDetailsButtons.add(btnDelete);

		JPanel panelNote = new JPanel();
		panelMain.add(panelNote, BorderLayout.CENTER);
		panelNote.setLayout(new BorderLayout(0, 0));

		textTitle = new JTextField();
		textTitle.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelNote.add(textTitle, BorderLayout.NORTH);
		textTitle.setColumns(10);

		textContent = new JTextArea();
		textContent.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelNote.add(textContent, BorderLayout.CENTER);

		JPanel panelLeft = new JPanel();
		splitPane.setLeftComponent(panelLeft);
		panelLeft.setLayout(new BorderLayout(0, 0));

		JPanel panelButtons = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panelButtons.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		panelLeft.add(panelButtons, BorderLayout.NORTH);

		JButton btnRefresh = new JButton("");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshList();
			}
		});
		btnRefresh.setIcon(new ImageIcon(CisneFrame.class
				.getResource("/com/Odroid/cisne/swing/img/refresh.png")));
		panelButtons.add(btnRefresh);

		JButton btnClean = new JButton("");
		btnClean.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cleanFilter();
			}
		});
		btnClean.setIcon(new ImageIcon(CisneFrame.class
				.getResource("/com/Odroid/cisne/swing/img/clean.png")));
		panelButtons.add(btnClean);

		JButton btnAdd = new JButton("");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNote();
			}
		});
		btnAdd.setIcon(new ImageIcon(CisneFrame.class
				.getResource("/com/Odroid/cisne/swing/img/add.png")));
		panelButtons.add(btnAdd);

		JPanel panelFilterList = new JPanel();
		panelLeft.add(panelFilterList, BorderLayout.CENTER);
		panelFilterList.setLayout(new BorderLayout(0, 0));

		textFilter = new JTextField();
		textFilter.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelFilterList.add(textFilter, BorderLayout.NORTH);
		textFilter.setColumns(10);

		JScrollPane scrollPanelList = new JScrollPane();
		panelFilterList.add(scrollPanelList, BorderLayout.CENTER);

		listNotes = new JList<NoteModel>();
		listNotes.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				NoteModel noteModel = listNotes.getSelectedValue();
				if (noteModel == null) {
					// showNote(null);
				} else {
					showNote(noteModel.getId());
				}
			}
		});
		listNotes.setFont(new Font("Tahoma", Font.PLAIN, 18));
		scrollPanelList.setViewportView(listNotes);
	}

	private void refreshList() {
		String filter = textFilter.getText();
		notesListModel.setFilter(filter);
	}

	private void cleanFilter() {
		textFilter.setText("");
		refreshList();
	}

	private void addNote() {
		Long id = notesService.addEmptyNote();
		notesListModel.setDirty();
		showNote(id);
	}

	private void deleteNote() {
		notesService.delete(showingId, showingVersion);
		notesListModel.setDirty();
		showNote(null);
	}

	private void saveNote() {
		Note note = new Note(showingId, textTitle.getText(),
				textContent.getText(), showingVersion);
		notesService.save(note);
		notesListModel.setDirty();
		showNote(showingId);
	}

	private void showNote(Long id) {
		if (id == null) {
			textTitle.setText("");
			textContent.setText("");
			showingId = -1L;
			showingVersion = 0L;
			if (panelMain.isVisible()) {
				mainWidth = panelMain.getWidth();
				int currentWidth = getWidth();
				int newWitdh = currentWidth - mainWidth;
				setSize(newWitdh, getHeight());
				panelMain.setVisible(false);
			}
		} else {
			Note note = notesService.get(id);
			textTitle.setText(note.getTitle());
			textContent.setText(note.getContent());
			showingId = id;
			showingVersion = note.getVersion();
			if (!panelMain.isVisible()) {
				int currentWidth = getWidth();
				int newWitdh = currentWidth + mainWidth;
				setSize(newWitdh, getHeight());
				panelMain.setVisible(true);
			}
		}
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			if (logonDialog.logon()) {
				super.setVisible(visible);
				refreshList();
			} else {
				System.exit(-1);
			}
		} else {
			super.setVisible(visible);
		}
	}

	private DocumentListener filterChangeListener = new DocumentListener() {

		@Override
		public void removeUpdate(DocumentEvent e) {
			refreshList();
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			refreshList();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			refreshList();
		}
	};
}
