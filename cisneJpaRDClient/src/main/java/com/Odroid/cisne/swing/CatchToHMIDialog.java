package com.Odroid.cisne.swing;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CatchToHMIDialog {
	private final Log logger = LogFactory.getLog(getClass());

	@Pointcut(value = "execution(* com.Odroid.cisne.blRest..*(..))")
	public void anyBlMethod() {
	}

	@Around("anyBlMethod()")
	public Object process(ProceedingJoinPoint jointPoint)
			throws Throwable {
		try {
			Object res = jointPoint.proceed();
			return res;
		} catch (RuntimeException e) {
			logger.error(e.getMessage(), e);
			String message = "<html><body><p style='width: 600px;'>"
					+ e.getMessage() + "</p></body></html>";
			JOptionPane.showMessageDialog(null, message, "Error",
					JOptionPane.ERROR_MESSAGE);
			throw e;
		}
	}
}
