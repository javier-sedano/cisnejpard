package com.Odroid.cisne.swing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.Odroid.cisne.blRest.NotesService;
import com.Odroid.cisne.domain.NoteMeta;
import com.Odroid.cisne.fw.swing.CachedListModel;
import com.Odroid.cisne.swing.NotesListModel.NoteModel;

@Component("notesListModel")
public class NotesListModel extends CachedListModel<NoteModel> {
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("notesService")
	private NotesService notesService;

	private String filter = "";
	private int size = 0;

	@Override
	public int getModelSize() {
		return size;
	}

	@Override
	public NoteModel getModelElementAt(int index) {
		NoteMeta note = notesService.searchGet(filter, (long) index);
		NoteModel model = new NoteModel(note.getId(), note.getTitle());
		return model;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
		setDirty();
	}

	@Override
	public void setDirty() {
		Long count = notesService.searchCount(filter);
		this.size = count.intValue();
		super.setDirty();
	}
	
	public static class NoteModel {
		private Long id;
		private String title;

		public NoteModel() {
			super();
		}

		public NoteModel(Long id, String title) {
			super();
			this.id = id;
			this.title = title;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		@Override
		public String toString() {
			return "NoteModel [id=" + id + ", title=" + title + "]";
		}

	}

}
