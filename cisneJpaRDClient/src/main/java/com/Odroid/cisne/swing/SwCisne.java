package com.Odroid.cisne.swing;

import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SwCisne {
	public static void main(String[] args) {
		final ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
				"com/Odroid/cisne/spring-context.xml",
				"com/Odroid/cisne/blRest/spring-bl-context.xml",
				"com/Odroid/cisne/swing/spring-swing-context.xml");
		JFrame mainFrame = ctx.getBean("cisneFrame", JFrame.class);
		mainFrame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent winEvt) {
				System.exit(0);
			}
		});

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				ctx.close();
			}
		});
		mainFrame.setVisible(true);
	}
}
