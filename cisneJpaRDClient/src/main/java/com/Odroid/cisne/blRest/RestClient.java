package com.Odroid.cisne.blRest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.Odroid.cisne.bl.IRestClient;

@Service("restClient")
public class RestClient implements IRestClient {
	private final Log logger = LogFactory.getLog(getClass());

	@Value("${server}")
	private String server;

	@Value("${server.echo}")
	private String echo;

	private RestTemplate restTemplate = null;

	@Override
	public boolean setCredentials(String username, String password) {
		try {
			RestTemplate restTemplate = buildRestTemplate(username, password);
			Map<String, Object> echoParams = new HashMap<>();
			echoParams.put("text", "qwerty");
			logger.info("Trying logon for user " + username + "/" + password);
			String resp = restTemplate.getForObject(server + echo,
					String.class, echoParams);
			if (!resp.equals("qwerty")) {
				logger.warn("Probably wrong user password");
				return false;
			}
			this.restTemplate = restTemplate;
			return username.equals(password);
		} catch (URISyntaxException e) {
			logger.error(e.getMessage(), e);
			return false;
		} catch (RestClientException e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}

	private RestTemplate buildRestTemplate(String username, String password)
			throws URISyntaxException {
		URI uri = new URI(server);
		String host = uri.getHost();
		int port = uri.getPort();
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(new AuthScope(host, port),
				new UsernamePasswordCredentials(username, password));
		CloseableHttpClient httpclient = HttpClients.custom()
				.setDefaultCredentialsProvider(credsProvider).build();
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(
				httpclient);
		RestTemplate restTemplate = new RestTemplate(factory);
		return restTemplate;
	}

	@Override
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

}
