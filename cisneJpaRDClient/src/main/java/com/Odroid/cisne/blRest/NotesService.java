package com.Odroid.cisne.blRest;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.bl.IRestClient;
import com.Odroid.cisne.domain.Note;
import com.Odroid.cisne.domain.NoteMeta;

@Service("notesService")
public class NotesService implements INotesService {
	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	@Qualifier("restClient")
	private IRestClient restClient;

	@Value("${server}")
	private String serverUrl;

	@Value("${server.notes.searchCount}")
	private String searchCountUrl;

	@Value("${server.notes.searchGet}")
	private String searchGetUrl;

	@Value("${server.notes.addEmpty}")
	private String addEmptyUrl;

	@Value("${server.notes.get}")
	private String getUrl;

	@Value("${server.notes.save}")
	private String saveUrl;

	@Value("${server.notes.delete}")
	private String deleteUrl;

	@Override
	public Long searchCount(String text) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("filter", text);
		logger.debug("Requesting count of notes for filter " + text);
		Long res = restClient.getRestTemplate().getForObject(
				serverUrl + searchCountUrl, Long.class, params);
		logger.debug("Requesting count of notes for filter " + text + " got "
				+ res);
		return res;
	}

	@Override
	public NoteMeta searchGet(String text, Long index) {
		// NOTA: estoy pidiendo una nota entera cuando podr�a pedir solo el
		// titulo, pero el API no me permite mas
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("filter", text);
		params.put("index", index);
		logger.debug("Requesting note for filter " + text + " in position "
				+ index);
		Note res = restClient.getRestTemplate().getForObject(
				serverUrl + searchGetUrl, Note.class, params);
		logger.debug("Requesting note for filter " + text + " in position "
				+ index + " got " + res.toString());
		NoteMeta n = new NoteMeta();
		n.setId(res.getId());
		n.setTitle(res.getTitle());
		return n;
	}

	@Override
	public Long addEmptyNote() {
		logger.debug("Requesting add empty note");
		Long res = restClient.getRestTemplate().getForObject(
				serverUrl + addEmptyUrl, Long.class);
		logger.debug("Requesting add empty note created " + res);
		return res;
	}

	@Override
	public Note get(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		logger.debug("Requesting note " + id);
		Note note = restClient.getRestTemplate().getForObject(
				serverUrl + getUrl, Note.class, params);
		logger.debug("Requesting note " + id + " got " + note.toString());
		return note;
	}

	@Override
	public void save(Note note) {
		logger.debug("Saving note " + note.toString());
		Long res = restClient.getRestTemplate().postForObject(
				serverUrl + saveUrl, note, Long.class);
		logger.debug("Saving note " + note.toString() + " got " + res);
	}

	@Override
	public void delete(Long id, Long version) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		params.put("version", version);
		logger.debug("Deleting note " + id + " @" + version);
		Long res = restClient.getRestTemplate().getForObject(
				serverUrl + deleteUrl, Long.class, params);
		logger.debug("Deleting note " + id + " got " + res);
	}
}
