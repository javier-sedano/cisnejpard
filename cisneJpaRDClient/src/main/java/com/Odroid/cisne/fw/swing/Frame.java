package com.Odroid.cisne.fw.swing;

import org.springframework.stereotype.Component;

@Component
public @interface Frame {
	/**
	 * The value may indicate a suggestion for a logical component name, to be
	 * turned into a Spring bean in case of an autodetected component.
	 * 
	 * @return the suggested component name, if any
	 */
	String value() default "";
}
