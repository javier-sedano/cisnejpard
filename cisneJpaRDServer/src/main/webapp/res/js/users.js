var users = {};

$(document).ready(function() {
	users.refreshList();
});

users.context = context;

users.refreshList = function() {
	var onFadeOutComplete = function() {
		var url = users.context + "usersList.html" + "?r="
				+ (new Date()).getTime();
		var onLoadComplete = fw.ajax.loadComplete(
				fw.i18n.s["users.error.list"], function(response, status, xhr) {
					$('#users_list').fadeIn(conf_anim_fast);
				});
		$('#users_list').load(url, onLoadComplete);
	};
	$('#users_list').fadeOut(conf_anim_fast, onFadeOutComplete);
};

users.show = function(id) {
	var onFadeOutComplete = function() {
		var url = users.context + "userDetails.html?id=" + id + "&r="
				+ (new Date()).getTime();
		var onLoadComplete = fw.ajax.loadComplete(
				fw.i18n.s["users.error.load"], function(response, status, xhr) {
					$('#users_details').fadeIn(conf_anim_fast);
				});
		$('#users_details').load(url, onLoadComplete);
	};
	$('#users_details').fadeOut(conf_anim_fast, onFadeOutComplete);
};

users.details_edit = function() {
	$('#users_details_view').fadeOut(conf_anim_fast, function() {
		$('#users_details_edit').fadeIn(conf_anim_fast);
	});
};

users.details_save = function() {
	var id = $('#users_id').val();
	var username = $('#users_username').val();
	var name = $('#users_name').val();
	var password = $('#users_password').val();
	var version = $('#users_version').val();
	var enabled = 0;
	if ($('#users_enabled').is(':checked')) {
		enabled = 1;
	}
	var admin = $('#users_admin').is(':checked');
	var data = {
		id : id,
		username : username,
		name : name,
		password : password,
		enabled : enabled,
		admin : admin,
		version : version
	};
	$.post(users.context + "userSave", data).success(function(data) {
		users.refreshList();
		users.show(data);
	}).error(fw.ajax.error(fw.i18n.s["users.error.save"]));
};

users.details_cancel = function() {
	$('#users_details_edit').fadeOut(conf_anim_fast, function() {
		$('#users_details_view').fadeIn(conf_anim_fast);
	});
};

users.details_delete = function() {
	var id = $('#users_id').val();
	var version = $('#users_version').val();
	var data = {
		id : id,
		version : version
	};
	var url = users.context + "userDelete";
	var onPostSuccess = function(data) {
		users.refreshList();
		var onFadeOutComplete = function() {
			$('#users_details').text($('#users_details_empty').text());
			$('#users_details').fadeIn(conf_anim_fast);
		};
		$('#users_details').fadeOut(conf_anim_fast, onFadeOutComplete);
	};
	var onPostError = fw.ajax.error(fw.i18n.s["users.error.delete"]);
	$.post(url, data).success(onPostSuccess).error(onPostError);
};

users.add = function() {
	var onFadeOutComplete = function() {
		var url = users.context + "userAdd.html" + "?r="
				+ (new Date()).getTime();
		var onLoadComplete = fw.ajax.loadComplete(
				fw.i18n.s["users.error.addForm"], function(response, status,
						xhr) {
					$('#users_details').fadeIn(conf_anim_fast);
				});
		$('#users_details').load(url, onLoadComplete);
	};
	$('#users_details').fadeOut(conf_anim_fast, onFadeOutComplete);
};

users.add_cancel = function() {
	$('#users_add').fadeOut(conf_anim_fast, function() {
		$('#users_details').text($('#users_details_empty').text());
		$('#users_details').fadeIn(conf_anim_fast);
	});
};

users.add_save = function() {
	var id = -1;
	var username = $('#users_username').val();
	var name = $('#users_name').val();
	var password = $('#users_password').val();
	var enabled = 0;
	if ($('#users_enabled').is(':checked')) {
		enabled = 1;
	}
	var admin = $('#users_admin').is(':checked');
	var data = {
		id : id,
		username : username,
		name : name,
		password : password,
		enabled : enabled,
		admin : admin
	};
	var onPostSuccess = function(data) {
		users.refreshList();
		users.show(data);
	};
	var onPostError = fw.ajax.error(fw.i18n.s["users.error.add"]);
	$.post(users.context + "userSave", data).success(onPostSuccess).error(
			onPostError);
};
