var notes = {};
$(document).ready(function() {
	$('#notes_filter').keyup(notes.refreshList);
	notes.refreshList();
});

notes.context = context;

notes.refreshList = function() {
	var onFadeOutComplete = function() {
		var q = $('#notes_filter').val();
		var url = notes.context + "notesSearch.html" + "?q=" + q + "&r="
				+ (new Date()).getTime();
		var onLoadComplete = fw.ajax.loadComplete(
				fw.i18n.s["notes.error.list"], function(response, status, xhr) {
					$('#notes_list').fadeIn(conf_anim_fast);
				});
		$('#notes_list').load(url, onLoadComplete);
	};
	$('#notes_list').fadeOut(conf_anim_fast, onFadeOutComplete);
};

notes.cleanFilter = function() {
	$('#notes_filter').val("");
	notes.refreshList();
};

notes.show = function(id) {
	var onFadeOutComplete = function() {
		var url = notes.context + "notesDetails.html?id=" + id + "&r="
				+ (new Date()).getTime();
		var onLoadComplete = fw.ajax.loadComplete(
				fw.i18n.s["notes.error.load"], function(response, status, xhr) {
					$('#notes_details').fadeIn(conf_anim_fast);
				});
		$('#notes_details').load(url, onLoadComplete);
	};
	$('#notes_details').fadeOut(conf_anim_fast, onFadeOutComplete);
};

notes.add = function() {
	var url = notes.context + "notesAddEmpty";
	var onPostSuccess = function(data) {
		var id = data;
		notes.refreshList();
		notes.show(id);
	};
	var onPostError = fw.ajax.error(fw.i18n.s["notes.error.add"]);
	$.get(url).success(onPostSuccess).error(onPostError);

};

notes.details_delete = function() {
	var id = $('#notes_id').val();
	var version = $('#notes_version').val();
	// (https://bugs.eclipse.org/bugs/show_bug.cgi?id=351470)
	// This hack removes the warning :-(
	var kk = id;
	kk = version;
	delete kk;
	var onFadeOutComplete = function() {
		$('#notes_details').text($('#notes_details_empty').text());
		$('#notes_details').fadeIn(conf_anim_fast);
		var data = {
			id : id,
			version : version
		};
		var url = notes.context + "notesDelete";
		var onPostSuccess = function(data) {
			notes.refreshList();
		};
		var onPostError = fw.ajax.error(fw.i18n.s["notes.error.delete"]);
		$.post(url, data).success(onPostSuccess).error(onPostError);
	};
	$('#notes_details').fadeOut(conf_anim_fast, onFadeOutComplete);
};

notes.details_save = function() {
	var id = $('#notes_id').val();
	var title = $('#notes_title').val();
	var content = $('#notes_content').val();
	var version = $('#notes_version').val();
	var data = {
		id : id,
		title : title,
		content : content,
		version : version
	};
	var url = notes.context + "notesSave";
	var onPostSuccess = function(data) {
		notes.refreshList();
		notes.show(id);
	};
	var onPostError = fw.ajax.error(fw.i18n.s["notes.error.save"]);
	$.post(url, data).success(onPostSuccess).error(onPostError);
};

notes.details_templates = function() {
	var content = $('#notes_content').val();
	var param = {
		content : content
	};
	fw.nav.ask("templates.html", param, true, notes.details_templates_callback);
};

notes.details_templates_callback = function(response) {
	if (response === null) {
		// Canceled
	} else {
		$('#notes_content').val(response);
	}
};
