<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<ul>
	<c:forEach items="${notes}" var="note">
		<li><a href="#" onclick="notes.show(${note.id}); return false;">${note.title}</a></li>
	</c:forEach>
</ul>
