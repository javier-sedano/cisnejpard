<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="notes_details_view">
	<a href="#" onclick="notes.details_save(); return false;"
		class="buttonImg"><img src="<c:url value="/res/img/accept.png"/>"
		alt="<spring:message code="notes.details.save" />"
		title="<spring:message code="notes.details.save" />" /></a> <a href="#"
		onclick="notes.details_delete(); return false;" class="buttonImg"><img
		src="<c:url value="/res/img/delete.png"/>"
		alt="<spring:message code="notes.details.delete" />"
		title="<spring:message code="notes.details.delete" />" /></a> <a href="#"
		onclick="notes.details_templates(); return false;" class="buttonImg"><img
		src="<c:url value="/res/img/templates.png"/>"
		alt="<spring:message code="notes.details.templates" />"
		title="<spring:message code="notes.details.templates" />" /></a> <br />
  <input type="hidden" id="notes_id" name="id" value="${note.id}">
  <input type="hidden" id="notes_version" name="version" value="${note.version}">
	<input name="title" type="text" size="40" value="${note.title}"
		id="notes_title" /> <br />
	<textarea rows="7" cols="80" name="content" id="notes_content">${note.content}</textarea>
	<br />

</div>