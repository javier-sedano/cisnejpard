<%@ include file="../bits/html-head.jsp" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="<c:url value="/res/js/users.js"/>"></script>

<div id="main">
	<%@ include file="../bits/header.jsp"%>

	<div id="site_content">
		<div class="leftbar">
			<h3>
				<spring:message code="users.list.header" />
			</h3>
			<p>
				<a href="#" onclick="users.refreshList(); return false;"
					class="buttonImg"> <img
					src="<c:url value="/res/img/refresh.png"/>"
					alt="<spring:message code="users.list.refresh" />"
					title="<spring:message code="users.list.refresh" />" />
				</a> <a href="#" onclick="users.add(); return false;" class="buttonImg">
					<img src="<c:url value="/res/img/add.png"/>"
					alt="<spring:message code="users.list.add" />"
					title="<spring:message code="users.list.add" />" />
				</a>
			</p>
			<div id="users_list"></div>
		</div>
		<div id="content">
			<h1>
				<spring:message code="users.detail.header" />
			</h1>
			<div id="users_details_empty" style="display: none">
				<p>
					<spring:message code="users.detail.empty" />
				</p>
			</div>
			<div id="users_details">
				<p>
					<spring:message code="users.detail.empty" />
				</p>
			</div>
		</div>
	</div>

</div>

<%@ include file="../bits/html-foot.jsp"%>
