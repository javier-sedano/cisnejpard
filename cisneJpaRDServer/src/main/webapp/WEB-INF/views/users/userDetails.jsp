<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="users_details_view">

	<p>
		<a href="#" onclick="users.details_edit(); return false;"
			class="buttonImg"><img src="<c:url value="/res/img/edit.png"/>"
			alt="<spring:message code="users.details.edit" />"
			title="<spring:message code="users.details.edit" />" /></a> <a href="#"
			onclick="users.details_delete(); return false;" class="buttonImg"><img
			src="<c:url value="/res/img/delete.png"/>"
			alt="<spring:message code="users.details.delete" />"
			title="<spring:message code="users.details.delete" />" /></a>
	</p>

	<p>
		<spring:message code="users.details.username" />
		${user.username}
	</p>
	<p>
		<spring:message code="users.details.name" />
		${user.name}
	</p>
	<p>
		<spring:message code="users.details.password" />
		${user.password}
	</p>
	<p>
		<spring:message code="users.details.enabled" />
		<c:choose>
			<c:when test="${user.enabled==true}">
				<spring:message code="users.details.yes" />
			</c:when>
			<c:otherwise>
				<spring:message code="users.details.no" />
			</c:otherwise>
		</c:choose>
	</p>
	<p>
		<spring:message code="users.details.admin" />
		<c:choose>
			<c:when test="${isAdmin}">Yes</c:when>
			<c:otherwise>No</c:otherwise>
		</c:choose>
	</p>
</div>

<div id="users_details_edit" style="display: none;">
	<p>
		<a href="#" onclick="users.details_save(); return false;"
			class="buttonImg"><img
			src="<c:url value="/res/img/accept.png" />"
			alt="<spring:message code="users.details.save" />"
			title="<spring:message code="users.details.save" />" /></a> <a href="#"
			onclick="users.details_cancel(); return false;" class="buttonImg"><img
			src="<c:url value="/res/img/cancel.png"/>"
			alt="<spring:message code="users.details.cancel" />"
			title="<spring:message code="users.details.cancel" />" /></a>
	</p>
	<input type="hidden" id="users_id" name="id" value="${user.id}">
	<input type="hidden" id="users_version" name="version"
		value="${user.version}">
	<p>
		<spring:message code="users.details.username" />
		<input type="text" id="users_username" name="username"
			value="${user.username}">
	</p>
	<p>
		<spring:message code="users.details.name" />
		<input type="text" id="users_name" name="name" value="${user.name}">
	</p>
	<p>
		<spring:message code="users.details.password" />
		<input type="text" id="users_password" name="password" value="">
		(Let it empty for no change)
	</p>
	<p>
		<spring:message code="users.details.enabled" />
		<input type="checkbox" id="users_enabled" name="admin"
			<c:if test="${user.enabled==true}">checked</c:if>>
	</p>
	<p>
		<spring:message code="users.details.admin" />
		<input type="checkbox" id="users_admin" name="admin"
			<c:if test="${isAdmin==true}">checked</c:if>>
	</p>
</div>
