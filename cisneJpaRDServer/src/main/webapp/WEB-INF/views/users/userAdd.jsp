<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="users_add">
	<p>
		<a href="#" onclick="users.add_save(); return false;"
			class="buttonImg"><img src="<c:url value="/res/img/accept.png"/>"
			alt="Accept" /></a> <a href="#"
			onclick="users.add_cancel(); return false;" class="buttonImg"><img
			src="<c:url value="/res/img/cancel.png"/>" alt="Cancel" /></a>
	</p>
	<p>
		<spring:message code="users.details.username" />
		<input type="text" id="users_username" name="username">
	</p>
	<p>
		<spring:message code="users.details.name" />
		<input type="text" id="users_name" name="name">
	</p>
	<p>
		<spring:message code="users.details.password" />
		<input type="text" id="users_password" name="password">
	</p>
	<p>
		<spring:message code="users.details.enabled" />
		<input type="checkbox" id="users_enabled" name="admin">
	</p>
	<p>
		<spring:message code="users.details.admin" />
		<input type="checkbox" id="users_admin" name="admin">
	</p>
</div>
