<%@ include file="../bits/html-head.jsp" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript"
	src="<c:url value="/res/js/templates.js"/>"></script>

<div id="main">
	<%@ include file="../bits/header.jsp"%>

	<div id="site_content">
		<h3>
			<spring:message code="templates.header" />
		</h3>
		<c:if test="${key != ''}">
			<a href="#" onclick="templates.templatifySentences(); return false;"
				class="buttonImg"><img
				src="<c:url value="/res/img/templatify_sentences.png"/>"
				alt="<spring:message code="templates.sentences" />"
				title="<spring:message code="templates.sentences" />" /></a>
			<a href="#" onclick="templates.templatifyWords(); return false;"
				class="buttonImg"><img
				src="<c:url value="/res/img/templatify_words.png"/>"
				alt="<spring:message code="templates.words" />"
				title="<spring:message code="templates.words" />" /></a>
			<a href="#" onclick="templates.templatifyAccept(); return false;"
				class="buttonImg"><img
				src="<c:url value="/res/img/accept.png"/>"
				alt="<spring:message code="common.accept" />"
				title="<spring:message code="common.accept" />" /></a>
			<br />
		</c:if>
		<spring:message code="templates.before" />
		<br /> <input type="hidden" id="key" value="${key}" />
		<pre id="text">${text}</pre>
		<spring:message code="templates.after" />
		<br />
		<pre id="after">${text}</pre>
	</div>

</div>

<%@ include file="../bits/html-foot.jsp"%>