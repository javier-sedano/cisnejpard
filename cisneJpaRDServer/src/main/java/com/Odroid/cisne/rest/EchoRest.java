package com.Odroid.cisne.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("rest/echo")
public class EchoRest {

	@RequestMapping(value = "echo", method = RequestMethod.GET)
	public String echo(@RequestParam String text) {
		return text;
	}

}
