package com.Odroid.cisne.blJpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.domain.Note;

@Service("notesService")
@Transactional(readOnly = true)
public class NotesService implements INotesService {

	@PersistenceContext
	private EntityManager em;

	@Override
	@PreAuthorize("#userId == principal.id")
	public List<Note> search(String text, @P("userId") Long userId) {
		return em
				.createQuery(
						"FROM Note n "
								+ "WHERE ((LOWER(n.title) LIKE LOWER(:text)) OR (LOWER(n.content) LIKE LOWER(:text))) AND n.userId = :userId "
								+ "ORDER BY n.title", Note.class)
				.setParameter("text", "%" + text + "%")
				.setParameter("userId", userId).getResultList();
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("#userId == principal.id")
	public Long add(@P("userId") Long userId, String title, String content) {
		Note note = new Note();
		note.setId(null);
		note.setUserId(userId);
		note.setTitle(title);
		note.setContent(content);
		em.persist(note);
		return note.getId();
	}

	@Override
	@PostAuthorize("(returnObject == null) or (returnObject.userId == principal.id)")
	public Note get(Long id) {
		return em.find(Note.class, id);
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("(@notesService.get(#id) == null) or (@notesService.get(#id).userId == principal.id)")
	public void save(@P("id") Long id, String title, String content,
			Long version) {
		Note n = get(id, version);
		n.setTitle(title);
		n.setContent(content);
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("(@notesService.get(#id) == null) or (@notesService.get(#id).userId == principal.id)")
	public void delete(@P("id") Long id, Long version) {
		em.remove(get(id, version));
	}

	@Override
	public Long searchCount(String text, Long userId) {
		return em
				.createQuery(
						"SELECT COUNT (*) FROM Note n "
								+ "WHERE ((LOWER(n.title) LIKE LOWER(:text)) OR (LOWER(n.content) LIKE LOWER(:text))) AND n.userId = :userId ",
						Long.class).setParameter("text", "%" + text + "%")
				.setParameter("userId", userId).getSingleResult();
	}

	@Override
	public Note searchGet(String text, Long index, Long userId) {
		return em
				.createQuery(
						"FROM Note n "
								+ "WHERE ((LOWER(n.title) LIKE LOWER(:text)) OR (LOWER(n.content) LIKE LOWER(:text))) AND n.userId = :userId "
								+ "ORDER BY n.title", Note.class)
				.setParameter("text", "%" + text + "%")
				.setParameter("userId", userId)
				.setFirstResult(index.intValue()).setMaxResults(1)
				.getSingleResult();
	}

	private Note get(Long id, Long version) {
		Note n = get(id);
		if (n == null) {
			throw new OptimisticLockException("Deleted by another client");
		}
		if (n.getVersion() != version) {
			throw new OptimisticLockException("Modified by another client");
		}
		return n;
	}

}
