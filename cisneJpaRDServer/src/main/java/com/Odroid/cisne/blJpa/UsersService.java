package com.Odroid.cisne.blJpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.User;
import com.Odroid.cisne.fw.core.AuditableAnnotation;

@Service("usersService")
@Transactional(readOnly = true)
public class UsersService implements IUsersService {
	@SuppressWarnings("unused")
	private final Log logger = LogFactory.getLog(getClass());

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<User> getUsers() {
		return em.createQuery("FROM User ORDER BY username", User.class)
				.getResultList();
	}

	@Override
	public User getUser(Long id) {
		return em.find(User.class, id);
	}

	@Override
	public User getUser(String username) {
		return em
				.createQuery("FROM User WHERE username = :username", User.class)
				.setParameter("username", username).getSingleResult();
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@AuditableAnnotation
	public Long addUser(String username, String password, String name,
			boolean enabled, boolean admin) {
		if (password.isEmpty()) {
			throw new EmptyPasswordException();
		}
		User user = User.buildUser(username, password, name);
		user.setEnabled(enabled);
		user.setAdmin(admin);
		em.persist(user);
		return user.getId();
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@AuditableAnnotation
	public void saveUser(Long id, String username, String password,
			String name, boolean enabled, boolean admin, Long version) {
		User user = getUser(id, version);
		user.setUsername(username);
		user.setRawPassword(password);
		user.setName(name);
		user.setEnabled(enabled);
		user.setAdmin(admin);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@AuditableAnnotation
	public void deleteUser(long id, Long version) {
		User u = getUser(id, version);
		em.remove(u);
		// NOTE: performance problem. Get: 1 select del usuario. Borrado en
		// cascada: 1 select para las authorities, +1 por cada authority para el
		// usuario (LAZY ignorado), +1 por cada delete de authority. Borrado: 1
		// query
		// 7 queries donde deber�an ser 2
	}

	private User getUser(Long id, Long version) {
		User u = getUser(id);
		if (u == null) {
			throw new OptimisticLockException("Deleted by another client");
		}
		if (u.getVersion() != version) {
			throw new OptimisticLockException("Modified by another client");
		}
		return u;
	}

}
