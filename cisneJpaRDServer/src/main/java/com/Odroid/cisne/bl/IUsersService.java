package com.Odroid.cisne.bl;

import java.util.List;

import com.Odroid.cisne.domain.User;

public interface IUsersService {

	public List<User> getUsers();

	public User getUser(Long id);

	public User getUser(String username);

	public Long addUser(String username, String password, String name, boolean enabled, boolean admin);

	public void saveUser(Long id, String username, String password, String name, boolean enabled, boolean admin, Long version);

	public void deleteUser(long id, Long version);

	public static class EmptyPasswordException extends IllegalArgumentException {
		private static final long serialVersionUID = 1L;

		public EmptyPasswordException() {
			super();
		}

		public EmptyPasswordException(String message, Throwable cause) {
			super(message, cause);
		}

		public EmptyPasswordException(String s) {
			super(s);
		}

		public EmptyPasswordException(Throwable cause) {
			super(cause);
		}
	}

}
