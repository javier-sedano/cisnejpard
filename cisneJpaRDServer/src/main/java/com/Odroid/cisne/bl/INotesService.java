package com.Odroid.cisne.bl;

import java.util.List;

import com.Odroid.cisne.domain.Note;

public interface INotesService {
	public List<Note> search(String text, Long userId);

	public Long add(Long userId, String title, String content);

	public Note get(Long id);

	public void save(Long id, String title, String content, Long version);

	public void delete(Long id, Long version);

	public Long searchCount(String text, Long userId);

	public Note searchGet(String text, Long index, Long userId);

}
