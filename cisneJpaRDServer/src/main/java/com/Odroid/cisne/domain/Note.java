package com.Odroid.cisne.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.springframework.beans.factory.annotation.Configurable;

@Entity
@Table(name = "notes")
@Configurable
public class Note {
	@Id
	@Column
	@GeneratedValue
	private Long id = -1L;

	@Column
	private String title = "t";

	@Column
	private String content = "c";

	@Column
	private Long userId = -1L;

	@Version
	@Column
	private Long version = 0L;

	@Transient
	@PersistenceContext
	private EntityManager em;

	public Note() {
	}

	public Note(Long id, String title, String content, Long userId) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Note [id=" + id + ", title=" + title + ", content=" + content
				+ ", userId=" + userId + ", version=" + version + "]";
	}

}
