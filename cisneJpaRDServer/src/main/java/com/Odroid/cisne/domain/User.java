package com.Odroid.cisne.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configurable
@Entity
@Table(name = "users")
public class User implements Serializable {
	private static final Log logger = LogFactory.getLog(User.class);
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue
	private Long id = -1L;

	@Column
	private String username = "";

	@Column
	private String password = "";

	@Column
	private Integer enabled = 0;

	@Column
	private String name = "";

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = true)
	private List<Authority> authorities = new ArrayList<Authority>();

	@Version
	@Column
	private Long version = 0L;

	@Transient
	@Autowired
	@Qualifier("bcryptEncoder")
	private BCryptPasswordEncoder passwordEncoder;

	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		// Bug: si cambio el username, peta, porque es FK. Solucion: primero
		// quito todas las authorities, luego cambio el username, y luego las
		// anado otra vez... pero paso de hacerlo en un ejemplo
		if (!this.username.equals(username)) {
			throw new UnsupportedOperationException("Not yet implemented");
		}
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password="
				+ password + ", enabled=" + enabled + ", name=" + name
				+ ", authorities=" + authorities + ", version=" + version + "]";
	}

	public void setRawPassword(String rawPassword) {
		if ((rawPassword == null) || (rawPassword.isEmpty())) {
			return;
		}
		logger.debug("Setting password for user " + username);
		String encodedPassword = passwordEncoder.encode(rawPassword);
		this.password = encodedPassword;
	}

	// NOTA: podria poner mas parametros, pero quiero probar que puede tener
	// defaults
	public static User buildUser(String username, String password, String name) {
		User user = new User();
		user.id = null;
		user.username = username;
		user.name = name;
		user.setRawPassword(password);
		Authority userAuthority = new Authority(null, Authority.ROLE_USER, user);
		user.authorities.add(userAuthority);
		return user;
	}

	public void setEnabled(boolean enabled) {
		if (enabled) {
			this.enabled = 1;
		} else {
			this.enabled = 0;
		}
	}

	public boolean isEnabled() {
		return (this.enabled != 0);
	}

	public void setAdmin(boolean admin) {
		Authority adminAuthority = null;
		for (Authority authority : authorities) {
			if (authority.getAuthority().equals(Authority.ROLE_ADMIN)) {
				adminAuthority = authority;
			}
		}
		if (admin) {
			if (adminAuthority == null) {
				adminAuthority = new Authority(null, Authority.ROLE_ADMIN, this);
				authorities.add(adminAuthority);
				logger.debug("Requested admin for user " + username
						+ ". Did not have it: added.");
			}
		} else {
			if (adminAuthority != null) {
				authorities.remove(adminAuthority);
				logger.debug("Requested admin for user " + username
						+ ". Did have it: removed.");
			}
		}

	}

	public boolean isAdmin() {
		for (Authority authority : authorities) {
			if (authority.getAuthority().equals(Authority.ROLE_ADMIN)) {
				return true;
			}
		}
		return false;
	}
}
