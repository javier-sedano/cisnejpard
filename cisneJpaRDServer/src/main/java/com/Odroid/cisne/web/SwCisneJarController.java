package com.Odroid.cisne.web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Controller
public class SwCisneJarController {
	private final Log logger = LogFactory.getLog(getClass());
	private static final String JARPATH = "/public/swCisne.jar";

	private String swCisneJarResourcesPath = "/WEB-INF/clients/cisneJpaRDClient-*-exe.jar";

	@Autowired
	private ApplicationContext applicationContext;

	private Map<String, byte[]> swCisneJars = new HashMap<String, byte[]>();

	@RequestMapping(value = JARPATH, method = RequestMethod.GET)
	public void getSwCisneJar(HttpServletResponse response) throws IOException {
		// example: server=http://localhost:8080/cisne/rest/
		String server = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/rest/").build().toUriString();
		byte[] jar = buildSpecificJar(server);
		response.getOutputStream().write(jar);
	}

	private synchronized byte[] buildSpecificJar(String server)
			throws IOException {
		InputStream is = null;
		ZipInputStream zis = null;
		ByteArrayOutputStream baos = null;
		ZipOutputStream zos = null;
		try {
			byte[] jarBytes = swCisneJars.get(server);
			if (jarBytes == null) {
				logger.info("swCisne.jar for " + server
						+ " not cached; creating");
				Resource[] swCisneJarResources = applicationContext
						.getResources(swCisneJarResourcesPath);
				if (swCisneJarResources.length != 1) {
					throw new IOException("Expected 1 resource for "
							+ swCisneJarResourcesPath + " but found "
							+ swCisneJarResources.length);
				}
				is = swCisneJarResources[0].getInputStream();
				zis = new ZipInputStream(is);
				baos = new ByteArrayOutputStream();
				zos = new ZipOutputStream(baos);
				ZipEntry entry;
				while ((entry = zis.getNextEntry()) != null) {
					ZipEntry newEntry = new ZipEntry(entry.getName());
					zos.putNextEntry(newEntry);
					if (entry.getName().equals("server.properties")) {
						logger.info("Replacing server.properties");
						String content = "server=" + server + "\r\n";
						zos.write(content.getBytes());
					} else {
						IOUtils.copy(zis, zos);
					}
					zos.closeEntry();
				}
				zos.close();
				jarBytes = baos.toByteArray();
				swCisneJars.put(server, jarBytes);
			}
			return jarBytes;
		} finally {
			if (zos != null) {
				zos.close();
			}
			if (is != null) {
				is.close();
			}
			if (zis != null) {
				zis.close();
			}
		}
	}
}
