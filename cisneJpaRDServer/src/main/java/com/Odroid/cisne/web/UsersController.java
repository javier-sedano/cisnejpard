package com.Odroid.cisne.web;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.User;

@Controller
public class UsersController {

	@Autowired
	@Qualifier("usersService")
	private IUsersService userService;

	/**
	 * Simply selects the view to render by returning its name.
	 */
	@RequestMapping(value = "users.html", method = RequestMethod.GET)
	public String users(Model model) {
		model.addAttribute("tab", "users");
		return "WEB-INF/views/users/users.jsp";
	}

	@RequestMapping(value = "usersList.html", method = RequestMethod.GET)
	public String list(Model model) {
		List<User> users = userService.getUsers();
		model.addAttribute("usersList", users);
		return "WEB-INF/views/users/usersList.jsp";
	}

	@RequestMapping(value = "userDetails.html", method = RequestMethod.GET)
	public String details(Model model, @RequestParam(value = "id") Long id) {
		User user = userService.getUser(id);
		model.addAttribute("user", user);
		model.addAttribute("isAdmin", user.isAdmin());
		return "WEB-INF/views/users/userDetails.jsp";
	}

	@RequestMapping(value = "userAdd.html", method = RequestMethod.GET)
	public String add(Model model, Principal principal) {
		return "WEB-INF/views/users/userAdd.jsp";
	}

	@RequestMapping(value = "userDelete", method = RequestMethod.POST)
	public @ResponseBody
	Long delete(Model model, @RequestParam(value = "id") Long id,
			@RequestParam(value = "version") Long version) {
		userService.deleteUser(id, version);
		return 0L;
		// If it fails it will return an HTTP error
	}

	@RequestMapping(value = "userSave", method = RequestMethod.POST)
	public @ResponseBody
	Long save(
			Model model,
			@RequestParam(value = "id") Long id,
			@RequestParam(value = "username") String username,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "enabled") Boolean enabled,
			@RequestParam(value = "admin") Boolean admin,
			@RequestParam(value = "version", required = false, defaultValue = "0") Long version) {
		Long res = 0L;
		if (id < 0) {
			res = userService.addUser(username, password, name, enabled, admin);
		} else {
			userService.saveUser(id, username, password, name, enabled, admin,
					version);
			res = id;
		}
		return res;
		// If it fails it will return an HTTP error
	}

}
