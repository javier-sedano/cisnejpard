package com.Odroid.cisne.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.Note;
import com.Odroid.cisne.fw.core.FwUser;

@Controller
public class NotesController {

	@Autowired
	@Qualifier("notesService")
	private INotesService notesService;

	@Autowired
	@Qualifier("usersService")
	private IUsersService usersService;

	@RequestMapping(value = "notes.html", method = RequestMethod.GET)
	public String notes(Model model) {
		model.addAttribute("tab", "notes");
		return "WEB-INF/views/notes/notes.jsp";
	}

	@RequestMapping(value = "notesSearch.html", method = RequestMethod.GET)
	public String search(Model model, @RequestParam String q,
			Authentication principal) {
		Long userId = ((FwUser) principal.getPrincipal()).getId();
		List<Note> notes = notesService.search(q, userId);
		model.addAttribute("notes", notes);
		return "WEB-INF/views/notes/notesSearch.jsp";
	}

	@RequestMapping(value = "notesAddEmpty", method = RequestMethod.GET)
	public @ResponseBody
	Long addEmpty(Authentication principal) {
		Long userId = ((FwUser) principal.getPrincipal()).getId();
		Long id = notesService.add(userId, "Note" + System.currentTimeMillis(),
				"");
		return id;
		// If it fails it will return an HTTP error
	}

	@RequestMapping(value = "notesDetails.html", method = RequestMethod.GET)
	public String detail(Model model, @RequestParam Long id) {
		Note note = notesService.get(id);
		model.addAttribute("note", note);
		return "WEB-INF/views/notes/notesDetails.jsp";
	}

	@RequestMapping(value = "notesDelete", method = RequestMethod.POST)
	public @ResponseBody
	Long delete(@RequestParam Long id, @RequestParam Long version) {
		notesService.delete(id, version);
		return 0L;
		// If it fails it will return an HTTP error
	}

	@RequestMapping(value = "notesSave", method = RequestMethod.POST)
	public @ResponseBody
	Long save(Model model, @RequestParam Long id, @RequestParam String title,
			@RequestParam String content, @RequestParam Long version) {
		notesService.save(id, title, content, version);
		return id;
		// If it fails it will return an HTTP error
	}

}
