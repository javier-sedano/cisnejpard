package com.Odroid.cisne.fw.bl;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

@Deprecated
public class FwSession {
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_USER = "ROLE_USER";

	@Deprecated
	public static boolean hasRole(String role) {
		// TODO: inyectado en lugar de estatico
		Collection<? extends GrantedAuthority> auths = SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		for (GrantedAuthority grantedAuthority : auths) {
			if (grantedAuthority.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	public static void dumpRoles() {
		// TODO: inyectado en lugar de estatico
		Collection<? extends GrantedAuthority> auths = SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		for (GrantedAuthority grantedAuthority : auths) {
			System.out.println("Role: "+grantedAuthority.getAuthority());
		}
	}
	
}
