package com.Odroid.cisne.fw.core;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public class FwUser extends User {
	private static final long serialVersionUID = 1L;
	private Long id = -1L;

	public FwUser(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, Long id) {
		super(username, password, enabled, accountNonExpired,
				credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
	}

	public FwUser(String username, String password,
			Collection<? extends GrantedAuthority> authorities, Long id) {
		super(username, password, authorities);
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public static FwUser getCurrentUser() {
		return (FwUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
