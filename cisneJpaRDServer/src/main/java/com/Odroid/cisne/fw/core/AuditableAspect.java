package com.Odroid.cisne.fw.core;

import java.util.UUID;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component("auditableAspect")
@Aspect
public class AuditableAspect {
	private static final String AUDIT = "AUDIT-";

	@Pointcut(value = "execution(public * *(..))")
	public void anyPublicMethod() {
	}

	@Around("anyPublicMethod() && @annotation(auditableAnnotation)")
	public Object process(ProceedingJoinPoint jointPoint,
			AuditableAnnotation auditableAnnotation) throws Throwable {
		String prefix = auditableAnnotation.value();
		String uuid = UUID.randomUUID().toString();
		if (!prefix.equals("")) {
			prefix += ": ";
		}
		String description = jointPoint.toLongString();
		String args = "";
		for (Object arg : jointPoint.getArgs()) {
			args += "{" + arg.toString() + "};";
		}
		String auditPrefix = AUDIT + uuid + " " + prefix + description;
		System.out.println(auditPrefix + " " + args);
		Object res = jointPoint.proceed();
		System.out.println(auditPrefix + " --> "
				+ (res == null ? null : res.toString()));
		return res;
	}

}
