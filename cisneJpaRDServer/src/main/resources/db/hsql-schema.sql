DROP SCHEMA PUBLIC CASCADE

DROP TABLE meta_version IF EXISTS;
CREATE TABLE meta_version (
  version DECIMAL NOT NULL
);


DROP TABLE authorities IF EXISTS ;
CREATE TABLE authorities (
  id INTEGER IDENTITY NOT NULL,
  username VARCHAR(50) NOT NULL,
  authority VARCHAR(50) NOT NULL
);
ALTER TABLE authorities
  ADD UNIQUE (username, authority);

DROP TABLE users IF EXISTS;
CREATE TABLE users (
  id INTEGER IDENTITY NOT NULL,
  username VARCHAR(20) NOT NULL,
  password VARCHAR(100) NOT NULL,
  enabled INTEGER NOT NULL,
  name VARCHAR(100) NOT NULL,
  version INTEGER NOT NULL
);
ALTER TABLE users
  ADD UNIQUE (username);

INSERT INTO users(id, username, password, enabled, name, version) VALUES (1, 'a', '$2a$10$XwPZPyS4CuHf2ExThS627eKd418Fr.511nt2q7wWjCR/patpRpesO', 1, 'Admin', 0);
INSERT INTO authorities(username, authority) VALUES ('a', 'ROLE_ADMIN');
INSERT INTO authorities(username, authority) VALUES ('a', 'ROLE_USER');

DROP TABLE notes IF EXISTS;
CREATE TABLE notes (
  id INTEGER IDENTITY NOT NULL,
  title VARCHAR(40) NOT NULL,
  content VARCHAR(20000) NOT NULL,
  userId INTEGER NOT NULL,
  version INTEGER NOT NULL
);


ALTER TABLE authorities
  ADD FOREIGN KEY (username) 
  REFERENCES users (username);
ALTER TABLE notes
  ADD FOREIGN KEY (userId) 
  REFERENCES users (id);

