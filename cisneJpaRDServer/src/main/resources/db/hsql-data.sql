INSERT INTO users(id, username, password, enabled, name, version) VALUES (2, 'q', '$2a$10$sLWOIeHVsAawsfHU3buRpOzSRpHzQxIUqX4Unb8R1wn/GGH3nHCJm', 1, 'Bonifacio Buendia', 0);
INSERT INTO authorities(username, authority) VALUES ('q', 'ROLE_USER');
INSERT INTO users(id, username, password, enabled, name, version) VALUES (3, 'c', '$2a$10$hV3U6r9wWv2DC./WvTkcre/ipfFGPNDevGKx6Db6TyaLlhvQjPMUe', 0, 'Carlos Casado Castro', 0);
INSERT INTO authorities(username, authority) VALUES ('c', 'ROLE_USER');

INSERT INTO notes(title, content, userId, version) VALUES ('one', 'One ring to the darlk lord''s hand', 1, 0);
INSERT INTO notes(title, content, userId, version) VALUES ('lorem', 'Lorem ipsum dolor sit amet', 1, 0);
INSERT INTO notes(title, content, userId, version) VALUES ('digo', 'Donde voy a llegar', 1, 0);
INSERT INTO notes(title, content, userId, version) VALUES ('nothing', 'So close, no matter how far', 1, 0);
INSERT INTO notes(title, content, userId, version) VALUES ('two', 'One, two, three o''clock, four o''clock rock', 2, 0);
INSERT INTO notes(title, content, userId, version) VALUES ('hal', 'Dave', 3, 0);
