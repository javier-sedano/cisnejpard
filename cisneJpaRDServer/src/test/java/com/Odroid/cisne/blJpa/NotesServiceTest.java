package com.Odroid.cisne.blJpa;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.Note;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"/com/Odroid/cisne/blJpa/spring-bl-context.xml",
		"/com/Odroid/cisne/blJpa/spring-context.xml" })
// @TestExecutionListeners(listeners = {
// WithSecurityContextTestExcecutionListener.class })
@Transactional
public class NotesServiceTest {

	@Autowired
	@Qualifier("notesService")
	private INotesService notesService;

	@Autowired
	@Qualifier("usersService")
	private IUsersService usersService;

	@Test
	public void crudTest() {
		String title = "hey oh";
		String content = "let's go";
		Long id = notesService.add(userId, title, content);
		Note n2 = notesService.get(id);
		Long v2 = n2.getVersion();
		Assert.assertEquals(title, n2.getTitle());
		Assert.assertEquals(content, n2.getContent());
		Assert.assertEquals(userId, n2.getUserId());
		notesService.save(id, title + "2", content + "2", v2);
		Note n3 = notesService.get(id);
		Assert.assertEquals(title + "2", n3.getTitle());
		Assert.assertEquals(content + "2", n3.getContent());
		Assert.assertEquals(n2.getUserId(), n3.getUserId());
		List<Note> notes = notesService.search(title, userId);
		if (notes.size() < 1) {
			Assert.fail();
		}
		notesService.delete(id, n3.getVersion());
	}

	// @Test
	public void optimisticLockTest() {
		// TODO: esto no lanza la excepcion de OptimisticLock porque estan en la
		// misma transaccion... REPENSAR TEST con transacciones distintas, que
		// es lo que de verdad quiero probar
		String title = "one";
		String content = "two three o'clock";
		Long id = notesService.add(userId, title, content);
		Note n2 = notesService.get(id);
		Long v2 = n2.getVersion();
		Note n3 = notesService.get(id);
		Long v3 = n3.getVersion();
		notesService.save(n2.getId(), title + "2", content + "2", v2);
		notesService.save(n3.getId(), title + "3", content + "3", v3);
		notesService.delete(id, v3);
	}

	private Long userId = 0L;

	// TODO: BeforeClass?
	@Before
	public void addUser() {
		String username = "jsnow";
		String password = "qwerty";
		String name = "Jon Snow";
		Long id = usersService.addUser(username, password, name, true, true);
		userId = id;
	}

}
