package com.Odroid.cisne.blJpa;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"/com/Odroid/cisne/blJpa/spring-bl-context.xml",
		"/com/Odroid/cisne/blJpa/spring-context.xml" })
@Transactional
public class UsersServiceTest {

	@Autowired
	@Qualifier("usersService")
	private IUsersService usersService;

	@Autowired
	@Qualifier("bcryptEncoder")
	private BCryptPasswordEncoder bcryptEncoder;

	@Test
	public void crudTest() {
		String username = "jsnow";
		String password = "qwerty";
		String name = "Jon Snow";
		Long id = usersService.addUser(username, password, name, true, true);
		User u1 = usersService.getUser(id);
		Long v1 = u1.getVersion();
		Assert.assertEquals(username, u1.getUsername());
		Assert.assertEquals(name, u1.getName());
		Assert.assertTrue(u1.isEnabled());
		Assert.assertTrue(password + ": " + u1.getPassword(),
				bcryptEncoder.matches(password, u1.getPassword()));
		Assert.assertTrue(u1.isAdmin());
		// Save without changing password
		usersService.saveUser(id, username, "", name, false, false, v1);
		User u2 = usersService.getUser(id);
		Assert.assertFalse(u2.isAdmin());
		Assert.assertFalse(u2.isEnabled());
		usersService.deleteUser(id, u2.getVersion());
	}

}
